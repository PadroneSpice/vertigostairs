/*
 * Vertigo Camera Effect Attempt
 * by Sean Castillo
 */

float[] rec0 = {600, 250, 100, 300}; //xpos, ypos, width, height
float[] rec1 = {520, 280, 80, 240}; //boxes go smaller at a compounding 80%
float[] rec2 = {456, 305, 64, 192};
float[] rec3 = {414, 325, 51, 153};
float[] rec4 = {380, 340, 41, 122};
float[] rec5 = {355, 355, 32, 97};

float[] wall = {350, 250, 250, 300};
float[] floor = {350, 355, 30, 97};
int counter = 0;

void setup()
{
  size(900,900);
  frameRate(60);
}

void draw()
{
  counter++;
  background(50);

  if (counter >= 200 && counter < 350)  //must be difference of 150
  {
     moveRec(); 
  }
  if (counter >= 600)
  {
     reset();
  }
    
  strokeWeight(1.5);
  fill(20);
  rect(wall[0], wall[1], wall[2], wall[3]);
  
  fill(70);
  rect(floor[0], floor[1], floor[2], floor[3]);  
  
  fill(132, 74, 25);
  rect(rec5[0], rec5[1], rec5[2], rec5[3]);
  rect(rec4[0], rec4[1], rec4[2], rec4[3]);
  rect(rec3[0], rec3[1], rec3[2], rec3[3]);
  rect(rec2[0], rec2[1], rec2[2], rec2[3]);
  rect(rec1[0], rec1[1], rec1[2], rec1[3]);
  rect(rec0[0], rec0[1], rec0[2], rec0[3]);

}

void moveRec() //0=xpos, 1=ypos, 2=width, 3=height
{
  //expand wall
  wall[0] -= 0.5;
  wall[2] += 0.5;
  wall[3] += 0.5;
  wall[1] -= 0.25;
  
  //expand rec0
  rec0[2] += 0.2;
  rec0[3] += 0.5;
  //displace to compensate
  rec0[1] -= 0.25;
  
  //expand rec1
  rec1[2] += 0.2;
  rec1[3] += 0.48;
  //displace to compensate
  rec1[1] -= 0.25;
  //tuck under
  rec1[0] += 0.25;
  
  //expand rec2
  rec2[2] += 0.2;
  rec2[3] += 0.46;
  //displace to compensate
  rec2[1] -= 0.25;
  //tuck under
  rec2[0] += 0.45;
  
  //expand rec3
  rec3[2] += 0.2;
  rec3[3] += 0.44;
  //displace to compensate
  rec3[1] -= 0.25;
  //tuck under
  rec3[0] += 0.50;
  
  //expand rec4
  rec4[2] += 0.2;
  rec4[3] += 0.42;
  //displace to compensate
  rec4[1] -= 0.25;
  //tuck under
  rec4[0] += 0.55;
  
  //expand rec5
  rec5[2] += 0.2;
  rec5[3] += 0.40;
  //displace to compensate
  rec5[1] -= 0.25;
  //tuck under
  rec5[0] += 0.60;
  
  //expand floor
  floor[2] += 1;
  floor[3] += 0.4;
  //displace to compensate
  floor[0] -= 0.5;
  floor[1] -= 0.25;
}

void reset()
{
  counter = 0;
 rec0 = new float[]{600, 250, 100, 300}; //xpos, ypos, width, height
 rec1 = new float[] {520, 280, 80, 240}; //boxes go smaller at a compounding 80%
 rec2 = new float[] {456, 305, 64, 192};
 rec3 = new float[] {414, 325, 51, 153};
 rec4 = new float[] {380, 340, 41, 122};
 rec5 = new float[] {355, 355, 32, 97};

 wall = new float[] {350, 250, 250, 300};
 floor = new float[] {350, 355, 30, 97}; 
}